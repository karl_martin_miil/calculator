package enos.itcollege.ee.calculator;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.Toast;

public class CalculatorActivity extends AppCompatActivity {

    private static Context context;
    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        CalculatorActivity.context = getApplicationContext();

        mDetector = new GestureDetectorCompat(this, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                float distanceX = e2.getX() - e1.getX();
                float distanceY = e2.getY() - e1.getY();
                if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > 100 && Math.abs(velocityX) > 100) {
                    if (distanceX > 0) {
                        onSwipeRight();
                    } else {
                        onSwipeLeft();
                    }
                    return true;
                }
                return false;
            }


            public void onSwipeLeft() {
                //https://stackoverflow.com/questions/22454839/android-adding-simple-animations-while-setvisibilityview-gone
                Toast.makeText(context, "Swipe left", Toast.LENGTH_SHORT).show();
                final LinearLayout calcButtons = (LinearLayout) findViewById(R.id.main_buttons);
                moveLayoutsTo(calcButtons.getWidth() * -1);
            }

            public void onSwipeRight() {
                Toast.makeText(context, "Swipe right", Toast.LENGTH_SHORT).show();
                moveLayoutsTo(0);
            }

            private void moveLayoutsTo(long x) {
                if (!context.getResources().getBoolean(R.bool.is_landscape)) {
                    final LinearLayout calcButtons = (LinearLayout) findViewById(R.id.main_buttons);
                    final LinearLayout additionalCalcButtons = (LinearLayout) findViewById(R.id.additional_buttons);
                    calcButtons.animate().translationX(x)
                            .alpha(1.0f)
                            .setDuration(300);
                    additionalCalcButtons.animate().translationX(x)
                            .alpha(1.0f)
                            .setDuration(300);
                }
            }
        });

    }



    //https://developer.android.com/training/gestures/detector.html
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }
}
